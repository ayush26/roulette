('use strict');

const checkGameWinner = (gameState, userCode) => {
  const winConditions = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [6, 4, 2]];

  return winConditions.some(threeInARow => threeInARow.every(square => gameState.gameBoard[square] === userCode));
};

const handlePlayerTurn = async ({ data, gameSession, user, res }) => {
  /**
   * {
   *    data.pin point
   * }
   */
  const gameState = gameSession.state;
  const { players } = gameSession;
  if (gameState.currentTurn === user.id) {
    if (gameState.chances === 9) {
      gameSession.end();
      return res({
        error: true,
        message: 'game over',
      });
    }
    data.pinPoint = typeof data.pinPoint === 'string' ? Number(data.pinPoint) : data.pinPoint;

    if (gameState.gameBoard[data.pinPoint] !== 0) {
      return res({ error: true, message: 'Wrong turn' });
    }
    gameState.gameBoard[data.pinPoint] = gameState.playerMap[user.id];
    gameState.playerClickedAt = Date.now();
    const isWinner = checkGameWinner(gameState, gameState.playerMap[user.id]);

    if (isWinner) {
      res({ success: true });

      gameState.winner = user.id;
      // gameSession.notifyAll({
      //   event: 'gameStateUpdate',
      //   gameState,
      // });
      // await redis.setKeyEx(`GAME-${gameSession.id}`, JSON.stringify(gameSession), 86400);
      // await helper.setGameState(gameState, gameSession.id);

      gameSession.end();
    }

    const currentUserIndex = players.findIndex(x => x.id === user.id);
    if (currentUserIndex === players.length - 1) {
      gameState.currentTurn = players[0].id;
    } else {
      gameState.currentTurn = players[currentUserIndex + 1].id;
    }
    gameState.chances += 1;
    await helper.setGameState(gameState, gameSession.id);
    res({ success: true });
    gameSession.notifyAll({
      event: 'gameStateUpdate',
      gameState,
    });
    if (gameState.chances === 9) {
      res({ success: true });
      gameSession.end();
      return undefined;
    }
  } else {
    res({
      error: true,
      message: 'Not Your turn',
    });
  }
  return undefined;
};

const checkGameStatus = async (gameSession, gameState) => {
  if (Date.now() - gameState.playerClickedAt > 10000) {
    const { players } = gameSession;
    const currentUserIndex = players.indexOf(gameState.currentTurn);
    if (currentUserIndex === players.length - 1) {
      [gameState.currentTurn] = players;
    } else {
      gameState.currentTurn = players[currentUserIndex + 1];
    }
    gameState.playerClickedAt = Date.now();
    await helper.setGameState(gameState, gameSession.id);
    gameSession.notifyAll({
      event: 'gameStateUpdate',
      gameState,
    });
  }
};

module.exports = {
  handlePlayerTurn,
  checkGameStatus,
};
