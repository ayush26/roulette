'use strict';

const router = require('express').Router();

router.get('/', async (req, res) => {
  try {
    const games = await models.Game.findAll({
      where: {
        active: true,
      },
    });
    res.status(200).send({
      games: games.toJSON(),
      success: true,
    });
  } catch (error) {
    helper.catchReqError(error, res);
  }
});
