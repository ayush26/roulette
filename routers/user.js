'use strict';

const express = require('express');
const R = require('ramda');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const redis = require('../services/redis');
const dbUtils = require('../utils/dbHelper');
const { addToQueue } = require('../services/queue-worker');

const router = express.Router();

router.get('/addToGame', async (req, res) => {
  // let x = JSON.parse(await redis.getKey('soc-A7db4743ef8c4ccbaa48c375c476e87')).socketId;
  // console.log(x);
  // socketHelper.sendMessage(x, { hey: 'hey' });
  await addToQueue(req.state.user, { id: '8dadkadkakdkakdkadkkadk' });
  res.send({ success: true });
});

const getLastSeen = async state => {
  try {
    const chatSession = await helper.findOneRaw('ChatSession', {
      id: state.reqParams.chatId,
    });
    if (chatSession) {
      if (chatSession.owner === state.user.id || chatSession.stranger === state.user.id) {
        const otherUser = chatSession.owner === state.user.id ? chatSession.stranger : chatSession.owner;
        const lastSeen = JSON.parse(await redis.getKey(otherUser));
        if (lastSeen) {
          if (Date.now() - lastSeen.time <= 30000) {
            state.userOnline = true;
            state.lastSeen = null;
          } else {
            state.userOnline = false;
            state.lastSeen = lastSeen.time;
          }
        } else {
          state.userOnline = false;
          state.lastSeen = 0;
        }
      } else {
        return Promise.reject(R.merge(state, { error: true, message: 'Invalid Request' }));
      }
    } else {
      return Promise.reject(R.merge(state, { error: true, message: 'Chat Id not found' }));
    }
    return state;
  } catch (err) {
    console.log(err);
    return err;
  }
};

const createPublicId = async state => {
  const oldPublicId = await helper.findOneRaw('PublicId', {
    UserId: state.user.id,
  });

  if (oldPublicId && !state.reqBody.update) {
    return Promise.reject(
      R.merge(state, {
        error: true,
        message: 'Bad Request',
        userMessage: 'Enter valid data',
      })
    );
  }
  if (oldPublicId) {
    const newPublicId = await helper.updateRaw('PublicId', { id: dbUtils.getDbID() }, { id: oldPublicId.id });
    delete newPublicId.UserId;
    return R.merge(state, { publicId: newPublicId });
  }
  const publicId = await helper.createRaw('PublicId', { UserId: state.user.id });
  delete publicId.UserId;
  return R.merge(state, { publicId });
};

// eslint-disable-next-line
const sendEmail = state =>
  helper
    .createRecord(state, 'email', 'EmailVerification', {
      UserId: state.user.id,
    })
    .then(_state => {
      const mailOptions = {
        from: 'contact.teamofcode@gmail.com',
        to: _state.user.email,
        subject: 'Email verification for Incognito',
        text: _state.email.id,
      };
      helper.sendMail(mailOptions);
      return _state;
    });

const validateLoginReq = state => {
  const status = R.allPass([body => R.is(String, body.mobileNumber), body => R.is(String, body.password)])(
    state.reqBody
  );
  if (status) return Promise.resolve(state);
  else
    return Promise.reject({
      error: true,
      errorCode: 'ERR001',
      message: 'unformated data',
      httpCode: 400,
    });
};

const getUser = async state => {
  const user = await models.User.findOne({
    where: {
      mobileNumber: state.reqBody.mobileNumber,
    },
  });
  return user;
};

const verifyUser = (user, state) => {
  if (user) {
    if (bcrypt.compareSync(state.reqBody.password, user.password)) {
      delete user.password;
      return Promise.resolve(state);
    }
  }
  return Promise.reject({
    error: true,
    errorCode: 'ERR002',
    message: 'Wrong credentials',
    httpCode: 400,
  });
};

const createUser = state => {
  const user = {
    name: state.reqBody.name,
    email: state.reqBody.email,
    password: state.reqBody.password,
    mobileNumber: state.reqBody.mobileNumber,
    age: state.reqBody.age,
  };
  return models.User.create(user);
};

const checkPreviousUser = async state => {
  const user = await models.User.findOne({
    where: {
      mobileNumber: state.reqBody.mobileNumber,
    },
  });
  if (user) {
    return Promise.reject({
      error: true,
      message: 'user already exists',
      userMessage: 'user already exists',
      errorCode: 'ERR001',
      httpCode: 400,
    });
  }
  return false;
};

const validateRegisterReq = state => {
  console.log(state.reqBody);
  const status = R.allPass([
    body => R.is(String, body.mobileNumber),
    body => R.is(String, body.password) && body.password.length >= 8,
    body => R.is(String, body.name),
    // body => R.is(Number, body.age) && body.age > 18,
  ])(state.reqBody);
  if (status) return true;
  return Promise.reject({
    error: true,
    message: 'Bad Request',
    userMessage: 'Enter valid data',
    httpCode: 400,
  });
};

const postTxn = (state, error) => {
  if (!error) {
    const newState = helper.getWhitelistedKeys(state, {
      whiteList: ['message', 'status', 'user', 'publicId', 'userOnline', 'lastSeen', 'token', 'link'],
      blackList: [['user', 'password']],
      default: 'BLACKLISTALL',
    });
    return Promise.resolve({ data: newState });
  }
  if (state.error) {
    const newState = helper.getWhitelistedKeys(state, {
      whiteList: ['error', 'message', 'errorCode', 'status'],
      default: 'BLACKLISTALL',
    });
    return Promise.resolve({ error: newState });
  }
  return Promise.reject({
    error: {
      error: true,
      message: 'internal server error',
      userMessage: 'Something went wrong',
    },
  });
};

router.post('/register', async (req, res) => {
  try {
    await validateRegisterReq(req.state);
    await checkPreviousUser(req.state);
    const user = await createUser(req.state);
    const token = helper.getJWT({ id: user.id });
    res.cookie('__tn', token, { httpOnly: true });
    res.status(200).send({
      user: _.omit(user.toJSON(), ['id', 'password', 'updatedAt', 'createdAt']),
      success: true,
    });
  } catch (err) {
    helper.catchReqError(err, res);
  }
});

router.post('/login', async (req, res) => {
  try {
    await validateLoginReq(req.state);
    const user = await getUser(req.state);
    await verifyUser(user, req.state);
    const token = helper.getJWT({ id: user.id });
    res.cookie('__tn', token, { httpOnly: true });
    res.status(200).send({
      user: _.omit(user.toJSON(), ['password', 'updatedAt', 'createdAt']),
      success: true,
    });
  } catch (error) {
    helper.catchReqError(error, res);
  }
});

router.get('/', (req, res) => {
  res.status(200).send({
    user: _.omit(req.state.user.toJSON(), ['password', 'updatedAt', 'createdAt']),
    success: true,
  });
});

router.post('/signOut', (req, res) => {
  res.clearCookie('__tn');
  res.status(200).send({ success: true });
});

router.get('/lastSeen/:chatId', (req, res) => {
  req.state.reqParams = req.params;
  getLastSeen(req.state)
    .then(state => postTxn(state, false), state => postTxn(state, true))
    .then(
      state => {
        res.status(200).send(state);
      },
      state => {
        res.status(500).send(state);
      }
    );
});
/**
router.post('/heartbeat', (req, res) => {
  redis
    .setKey(
      req.state.user.id,
      JSON.stringify({
        fcmId: state.reqBody.token,
        time: Date.now(),
      })
    )
    .then(() => R.merge(state, { status: 'SUCCESS' }))
    .then(state => postTxn(state, false), state => postTxn(state, true))
    .then(
      state => {
        res.status(200).send(state);
      },
      state => {
        res.status(500).send(state);
      }
    );
});
*/

router.post('/createPublicId', (req, res) => {
  createPublicId(req.state)
    .then(async state => {
      delete state.user;
      state.link = `${global.config.serverLink}/startChat?id=${state.publicId.id}`;
      state.publicId = await helper.updateRaw('PublicId', { link: state.link }, { id: state.publicId.id });
      return state;
    })
    .then(state => postTxn(state, false), state => postTxn(state, true))
    .then(
      state => {
        res.status(200).send(state);
      },
      state => {
        res.status(500).send(state);
      }
    );
});

module.exports = router;
