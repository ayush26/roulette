const env = process.env.NODE_ENV || 'development';
const express = require('express');
const router = express.Router();
const R = require('ramda');
const helper = require('../utils/helper');
const bcrypt = require('bcryptjs');
const fcmHelper = require('../utils/firebaseX');
const redis = require('../services/redis');
const models = require('../db/models');
const moment = require('moment-timezone');

router.post('/sendMessage', (req, res) => {
  validateMessageReq(req.state)
    .then(getChatSession)
    .then(createMessageRecord)
    .then(checkUserOnline)
    .then(state => postTxn(state, false), state => postTxn(state, true))
    .then(
      state => {
        res.status(200).send(state);
      },
      state => {
        res.status(500).send(state);
      }
    );
});

router.get('/chats', (req, res) => {
  getAllChats(req.state)
    .then(state => postTxn(state, false), state => postTxn(state, true))
    .then(
      state => {
        res.status(200).send(state);
      },
      state => {
        res.status(500).send(state);
      }
    );
});

router.post('/createChatSession', (req, res) => {
  createChatSession(req.state)
    .then(state => postTxn(state, false), state => postTxn(state, true))
    .then(
      state => {
        res.status(200).send(state);
      },
      state => {
        res.status(500).send(state);
      }
    );
});

const getAllChats = async state => {
  let chatSessions = await findAllChatSessions(state, 'createdAt');
  let chatSessionIds = R.map(a => a.id, chatSessions);
  let messages = await models.sequelize.query(`SELECT DISTINCT ON ("ChatSessionId") *
    FROM "Messages" 
    where "ChatSessionId" in (${"'" + chatSessionIds.join("','") + "'"})
    ORDER BY "ChatSessionId","createdAt" DESC;`);
  chatSessions = R.map(chatSessionT => {
    let chatSession = chatSessionT.toJSON();
    chatSession['lastMessage'] = R.find(R.propEq('ChatSessionId', chatSession.id), messages[0]);
    if (chatSession.lastMessage)
      chatSession.lastMessage = deleteMsgKeysForGet(state, chatSession, chatSession.lastMessage);
    else chatSession['lastMessage'] = null;
    return chatSession;
  }, chatSessions);
  chatSessions = chatSessions.map(chatSession => deleteCriticalSessionData(chatSession, state.user.id));
  chatSessions.sort((a, b) => {
    if (a.lastMessage && b.lastMessage) {
      return moment(a.lastMessage.createdAt).diff(b.lastMessage.createdAt);
    }
    if (a.lastMessage && !b.lastMessage) return moment(a.lastMessage.createdAt).diff(b.createdAt);
    if (!a.lastMessage && b.lastMessage) return moment(a.createdAt).diff(b.lastMessage.createdAt);
    return moment(a.createdAt).diff(b.createdAt);
  });

  return R.merge(state, { chatSessions });
};

const findAllChatSessions = (state, orderBy) => {
  return models['ChatSession'].findAll({
    where: {
      $or: [
        {
          owner: state.user.id,
        },
        {
          stranger: state.user.id,
        },
      ],
    },
  });
};

const deleteCriticalMsgData = (state, chatSession, message) => {
  if (chatSession.owner == state.user.id) {
    //sender is owner
    delete message.deleteByOwner;
  } else {
    delete message.deleteByStranger;
  }
  if (message.from === 'OWNER' && chatSession.owner === state.user.id) {
    message['type'] = 'RECEIVE';
  } else {
    message['type'] = 'SENT';
  }
  return message;
};

const deleteMsgKeysForGet = (state, chatSession, message) => {
  if (chatSession.owner === state.user.id) {
    //sender is owner
    delete message.deleteByStranger;
  } else {
    delete message.deleteByOwner;
  }
  if (message.from === 'OWNER' && chatSession.owner === state.user.id) {
    message['type'] = 'SENT';
  } else {
    message['type'] = 'RECEIVE';
  }
  return message;
};

const deleteCriticalSessionData = (chatSession, userId) => {
  if (chatSession.owner === userId) {
    delete chatSession.stranger;
    delete chatSession.visibleToStranger;
    delete chatSession.owner;
  } else {
    delete chatSession.stranger;
    delete chatSession.owner;
    delete chatSession.strangerName;
    delete chatSession.visibleToOwner;
  }
  return chatSession;
};
const createChatSession = async state => {
  let publicId = await helper.findOneRaw('PublicId', {
    id: state.reqBody.userId,
  });
  let owner = publicId ? await helper.findOneRaw('User', { id: publicId.UserId }) : null;
  if (owner) {
    let oldChatSession = await helper.findOneRaw('ChatSession', {
      owner: owner.id,
      stranger: state.user.id,
    });
    if (oldChatSession) {
      return R.merge(state, {
        chatSession: deleteCriticalSessionData(oldChatSession, state.user.id),
      });
    }
    let chatSession = await helper.createRaw('ChatSession', {
      owner: owner.id,
      stranger: state.user.id,
      ownerName: owner.name,
    });
    delete chatSession.owner;
    delete chatSession.strangerName;
    delete chatSession.visibleToOwner;
    chatSession.otherName = chatSession.ownerName;
    chatSession.whoAmI = 'STRANGER';
    chatSession.visible = chatSession.visibleToStranger;
    chatSession['blockedByMe'] = chatSession.blockedByStranger;
    return R.merge(state, { chatSession });
  }
  return Promise.reject(state);
};

const checkUserOnline = state => {
  let otherGuy = state.chatSession.owner == state.user.id ? state.chatSession.stranger : state.chatSession.owner;
  return (
    redis
      .getKey(otherGuy)
      .then(val => {
        let data = JSON.parse(val);
        state['token'] = data.fcmId;
        return state;
      })
      // .catch(state =>{
      //   state.message.from = state.chatSession.owner == state.user.id ? "OWNER":"STRANGER";
      //   state['payload'] = {
      //     "message": {
      //       "token": otherGuy,
      //       "data": state.message
      //     }
      //   };
      //   state['token'] = data.fcmId;
      //   state.payload.message['notification']={
      //       "title":"You have a new message",
      //       "body":"Click to see"
      //   };
      // })
      .then(state => {
        let message = deleteCriticalMsgData(state, state.chatSession, R.clone(state.message));
        return fcmHelper.sendMessage(state, state.token, message);
      })
      .then(state => {
        state['message'] = deleteMsgKeysForGet(state, state.chatSession, state.message);
        delete state.chatSession;
        return state;
      })
      .catch(state => {
        state['message'] = deleteMsgKeysForGet(state, state.chatSession, state.message);
        delete state.chatSession;
        return Promise.resolve(state);
      })
  );
};

const createMessageRecord = state => {
  if (state.chatSession.owner == state.user.id || state.chatSession.stranger == state.user.id) {
    return helper.createRecord(state, 'message', 'Message', {
      from: state.chatSession.owner == state.user.id ? 'OWNER' : 'STRANGER',
      ChatSessionId: state.chatSession.id,
      messageText: state.reqBody.messageText,
      time: Date.now(),
    });
  }
  return Promise.reject(
    R.merge(state, {
      error: true,
      errorCode: 'ERR001',
      message: 'unformated data',
    })
  );
};

const getChatSession = state => {
  return helper.findOne(state, 'chatSession', 'ChatSession', {
    id: state.reqBody.chatSession,
  });
};

const validateMessageReq = state => {
  let status = R.allPass([body => R.is(String, body.messageText), body => R.is(String, body.chatSession)])(
    state.reqBody
  );
  if (status) return Promise.resolve(state);
  else
    return Promise.reject(
      R.merge(state, {
        error: true,
        errorCode: 'ERR001',
        message: 'unformated data',
      })
    );
};

const postTxn = (state, error) => {
  if (!error) {
    let newState = helper.getWhitelistedKeys(state, {
      whiteList: ['status', 'chatSession', 'message', 'chatSessions'],
      blackList: [['user', 'password'], ['user', 'id']],
      default: 'BLACKLISTALL',
    });
    return Promise.resolve({ data: newState });
  }
  if (state.error) {
    let newState = helper.getWhitelistedKeys(state, {
      whiteList: ['error', 'message', 'status', 'errorCode'],
      default: 'BLACKLISTALL',
    });
    return Promise.resolve({ error: newState });
  }
  return Promise.reject({
    error: true,
    message: 'internal server error',
    userMessage: 'Something went wrong',
  });
};

module.exports = router;
