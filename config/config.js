const config = {
  development: {
    secret_hash: 'ascrethash',
    s3Url: 'https://s3.ap-south-1.amazonaws.com/eventio-files-ayush',
    emailProviderHost: 'smtp.gmail.com',
    emailProviderUsername: 'contact.teamofcode@gmail.com',
    emailProviderPassword: 'tocqwerty',
    redis: {
      host: 'localhost',
      port: 16379,
      db: 0,
      socket_keepalive: true,
    },
    serverLink: 'http://localhost:5555',
    entryTime: 300 /** in seconds */,
  },
  staging: {
    secret_hash: 'ascrethash',
    s3Url: 'https://s3.ap-south-1.amazonaws.com/eventio-files-ayush',
    emailProviderHost: 'smtp.gmail.com',
    emailProviderUsername: 'contact.teamofcode@gmail.com',
    emailProviderPassword: 'tocqwerty',
    redis: {
      host: 'localhost',
      port: 16379,
      db: 0,
      socket_keepalive: true,
    },
  },
  production: {
    secret_hash: process.env.SECRETHASH,
    s3Url: 'assas',
  },
};

module.exports = config;
