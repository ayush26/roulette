'use strict';

const { EventEmitter } = require('events');
const redis = require('../services/redis');

class Player extends EventEmitter {
  constructor(player) {
    super();
    this.id = player.id;
    this.name = player.name;
    this.socketId = null;
  }

  async setSocketId() {
    let socketData = await redis.getKey(`soc-${this.id}`);
    socketData = socketData && JSON.parse(socketData);
    this.socketId = socketData && socketData.socketId;
  }

  send(msg) {
    socketHelper.sendMessage(this.socketId, msg);
  }
}

module.exports = Player;
