'use strict';

const { EventEmitter } = require('events');
const _ = require('lodash');
const Player = require('./Player');
const redis = require('../services/redis');
const ticTacToe = require('../games/tictactoe');

const STATUS = {
  QUEUED: 'QUEUED',
  COUNTDOWN: 'COUNTDOWN',
  INPROGRESS: 'INPROGRESS',
  OVER: 'OVER',
};
module.exports.STATUS = STATUS;

const activeGames = {};
module.exports.activeGames = activeGames;

class GameSession extends EventEmitter {
  constructor({ gameData, totalPLayers, gameSession }) {
    super();
    console.log('new GameSession');
    this.DURATION = 100;
    this.MAX_PLAYERS = gameData.maximumPlayers;
    this.MIN_PLAYERS = gameData.maximumPlayers;
    this.PREGAME_COUNTDOWN_DURATION = gameData.countDownTime;
    this.game = gameData;
    this.gameSession = gameSession;
    this.gameSession.type = gameData.type;
    this.id = gameSession.id;
    this.status = STATUS.QUEUED;
    const playerObjs = totalPLayers.map(player => new Player(player));
    this.players = playerObjs;
    this._periodicals = {};
    this.state = {};
    if (this.players.length === this.MAX_PLAYERS) {
      this.prepareGame();
    }
    activeGames[this.id] = this;
  }

  // set status(val) {
  //   this.gameSession.status = val;
  //   return redis.setKeyEx(`GAME-${this.id}`, JSON.stringify(this.gameSession), 86400);
  // }

  notify(msg, eventSource) {
    msg.args = msg.args || {};
    msg.args.gameSessionId = this.id;
    msg.args.status = this.status;
    this.players.forEach(player => {
      if (player.id !== eventSource) {
        player.send(msg);
      }
    });
  }

  async prepareGame() {
    await redis.setKeyEx(`GAME-${this.id}`, JSON.stringify(this.gameSession), 86400);
    if (this.game.type === 'TICTACTOE') {
      this.state = {
        currentTurn: this.players[0].id,
        chances: 0,
        gameBoard: [0, 0, 0, 0, 0, 0, 0, 0, 0],
        playerMap: {
          [this.players[0].id]: 1,
          [this.players[1].id]: 2,
        },
        playerClickedAt: Date.now(),
        winner: null,
      };
      this.opponentData = {
        [this.players[0].id]: _.pick(this.players[1], ['id', 'name']),
        [this.players[1].id]: _.pick(this.players[0], ['id', 'name']),
      };
      await helper.setGameState(this.state, this.id);
    }
    await Promise.all(this.players.map(player => player.setSocketId()));
    await this.countDown();
  }

  async countDown() {
    if (this.PREGAME_COUNTDOWN_DURATION === 0) {
      return this.start();
    }
    this.status = STATUS.COUNTDOWN;
    this.notifyAll({
      event: 'GAME_MATCHED',
      opponentData: this.opponentData,
    });
    return new Promise(resolve => {
      this.periodiclyNotify({
        method: 'GAME_COUNTDOWN',
        times: this.PREGAME_COUNTDOWN_DURATION,
        args: {
          status: this.status,
          gameSessionId: this.id,
        },
        howOften: 1000,
        onTick: () => {
          // console.log('game countdown: (%d)', times);
        },
        onComplete: () => {
          const self = this;
          setTimeout(() => {
            resolve(self.start());
          }, 1000);
        },
      });
    });
  }

  periodiclyNotify(options) {
    const { method, onTick, howOften, onComplete } = options;
    const args = options.args || {};
    let times = options.times || 1;
    const self = this;

    if (!method) throw new Error('`method` is required');
    if (!howOften) throw new Error('`howOften` is required');

    this.periodicalyNotifyStop(method);
    self._periodicals[method] = setInterval(() => {
      self.notify({ event: method, args: { ...args, times } });
      if (onTick) onTick(times);
      times -= 1;
      if (!times) {
        clearInterval(self._periodicals[method]);
        if (onComplete) onComplete();
      }
    }, howOften);
  }

  periodicalyNotifyStop(method) {
    if (this._periodicals[method]) clearInterval(this._periodicals[method]);
  }

  async start() {
    this.status = STATUS.INPROGRESS;

    this.state.playerClickedAt = Date.now();
    await helper.setGameState(
      {
        ...this.state,
      },
      this.id
    );
    this.periodicalyNotifyStop('GAME_COUNTDOWN');
    this.notify({
      event: 'START_GAME',
      gameState: this.state,
    });
    this.periodiclyCheckGameState({
      howOften: 1000,
      times: this.DURATION,
    });
  }

  periodiclyCheckGameState(options) {
    const { howOften } = options;
    let times = options.times || 1;
    const self = this;
    const method = 'checkGameState';

    if (!howOften) throw new Error('`howOften` is required');

    this.periodicalyNotifyStop(method);
    self._periodicals[method] = setInterval(async () => {
      // console.log('checking state ', times);
      // if (self.game.type === 'TICTACTOE') {
      //   ticTacToe.checkGameStatus(self, self.state);
      // }
      times -= 1;
      if (!times) {
        self.end();
      }
    }, howOften);
  }

  end() {
    this.status = STATUS.OVER;
    this.notify({
      event: 'GAME_OVER',
      gameState: this.state,
    });
    this.state.status = STATUS.OVER;
    clearInterval(this._periodicals.checkGameState);
  }

  notifyAll(msg) {
    msg.args = msg.args || {};
    msg.args.gameSessionId = this.id;
    msg.args.status = this.status;
    this.players.forEach(player => {
      player.send(msg);
    });
  }

  handlePlayerEvent({ data, user, res }) {
    if (data.event === 'TURN') {
      if (this.status === STATUS.OVER)
        res({
          error: true,
          message: 'Game Ended',
        });
      ticTacToe.handlePlayerTurn({ data, user, gameSession: this, res });
    }
  }
}
module.exports.GameSession = GameSession;

// GameSession.prototype.add = function(player) {
//   console.log('GameSession.add');

//   assert.ok(player instanceof Player);

//   this.players.push(player);

//   player.on('quit', this.remove.bind(this, player));
//   player.on('event', this.notify.bind(this));
//   player.on('gameover', this.end.bind(this));

//   this.notify({
//     method: 'gamejoin',
//     args: this.players.map(function(player) {
//       return player.uid;
//     }),
//   });

//   if (STATUS.QUEUED === this.status && this.MIN_PLAYERS === this.players.length) this.countdown();
//   else if (this.MAX_PLAYERS === this.players.length) this.start();
// };
/** 
GameSession.prototype.remove = function(who) {
  var players = this.players,
    len = players.length,
    whouid = who.uid,
    player;
  while (len-- && (player = players[len]))
    if (player.uid == whouid) {
      players.splice(len, 1);
      player.removeAllListeners('quit');
      this.emit('remove', player, players.length);
      console.log('player removed (%s)', whouid);
      if (STATUS.COUNTDOWN === this.status && players.length < this.MIN_PLAYERS) {
        self.periodicalyNotifyStop('GAME_COUNTDOWN');
        this.status = STATUS.QUEUED;
        this.emit('requeued');
      }
      console.log('remaining players in GameSession: ' + players.length);
      break;
    }
  return who;
};


GameSession.prototype.periodicalyNotifyStop = function(method) {
  if (this._periodicals[method]) clearInterval(this._periodicals[method]);
};

GameSession.prototype.periodiclyNotify = function(options) {
  var method = options.method,
    onTick = options.onTick,
    args = options.args || [],
    times = options.times || 1,
    howOften = options.howOften,
    onComplete = options.onComplete,
    self = this;

  if (!method) throw new Error('`method` is required');
  if (!howOften) throw new Error('`howOften` is required');

  this.periodicalyNotifyStop(method);

  self._periodicals[method] = setInterval(function() {
    var ary = cpyary(args);
    ary.push(times);
    self.notify({ method: method, args: ary });
    onTick && onTick(times);
    if (!--times) {
      clearInterval(self._periodicals[method]);
      onComplete && onComplete();
    }
  }, howOften);
};

// starts the GameSession
GameSession.prototype.start = function() {
  console.log('GameSession.start');

  this.status = STATUS.INPROGRESS;
  this.periodicalyNotifyStop('GAME_COUNTDOWN');

  this.add = function() {
    console.warn('Tried adding player to a GameSession in progress!');
  };

  // Notify all players that GameSession has started
  this.notify({ method: 'gamestart' });
  this.emit('start');

  this.periodiclyNotify({
    method: 'gametimer',
    howOften: 1000,
    times: this.DURATION,
    onComplete: this.end.bind(this),
  });
};

// ends the game
GameSession.prototype.end = function() {
  console.log('GameSession.end');
  this.periodicalyNotifyStop('gametimer');
  this.status = STATUS.OVER;
  this.notify({ method: 'gameover' });
  this.emit('end', this.players);
};
*/
