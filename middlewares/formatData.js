module.exports = function(req, res, next) {
  req.state = {
    reqParams: req.params,
    reqBody: req.body,
    reqQuery: req.query,
    reqHeaders: req.headers,
    originalUrl: req.originalUrl,
    reqMethod: req.method,
  };
  next();
};
