'use strict';

const R = require('ramda');
const helper = require('../utils/helper');

const checkNonAuthRoute = req => {
  // console.log(req);
  const status = R.anyPass([
    (path, method) => method === 'OPTIONS',
    (path, method) => (path === '/api/v1/users/register' || path === '/api/v1/users/register/') && method === 'POST',
    (path, method) => (path === '/api/v1/users/login' || path === '/api/v1/users/login/') && method === 'POST',
    path => path.indexOf('/api/') === -1,
  ])(req.originalUrl, req.method);
  return status;
};
const parseCookies = cookies => {
  const list = {};
  cookies.split(';').forEach(cookie => {
    const parts = cookie.split('=');
    list[parts.shift().trim()] = decodeURI(parts.join('='));
  });
  return list;
};

const getUser = async state => {
  const user = await models.User.findOne({
    where: {
      id: state.decoded.id,
    },
    attributes: {
      exclude: ['password', 'createdAt', 'updatedAt'],
    },
  });
  if (!user) return Promise.reject();
  state.user = user;
  return state;
};

module.exports = (req, res, next) => {
  if (checkNonAuthRoute(req)) {
    return next();
  } else {
    const cookies = parseCookies(req.headers.cookie);
    if (cookies && cookies.__tn) {
      if (!cookies.__tn) return res.status(401).send({ error: true, message: 'No token provided.' });
      return helper
        .verifyJWT(req.state, cookies.__tn)
        .then(getUser)
        .then(state => {
          req.state = state;
          next();
        })
        .catch(() => {
          res.status(401).send({
            error: {
              error: true,
              message: 'Authentication Problem',
            },
          });
        });
    } else {
      return res.status(401).send({ error: true, message: 'No token provided.' });
    }
  }

};
