const multer = require('multer');
const path = require('path');
// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//       cb(null, './tmp/uploads')
//   },
//   filename: function (req, file, cb) {
//       cb(null, file.originalname + '-' +Date.now()+path.extname(file.originalname));
//   }
// });
module.exports = function(req, res, next) {
  var upload = multer(
    {
      storage: multer.memoryStorage(),
      limits: {
        fileSize: 10485760,
      },
    },
    function(err) {
      console.log('erorrr');
    }
  ).any();
  upload(req, res, function(err) {
    if (err) {
      res.status(400).send({ error: true, message: 'File Unformated' });
    } else {
      req.state.reqBody = req.body;
      req.state.reqFiles = req.files;
      next();
    }
  });
};
