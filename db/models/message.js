const dbHelper = require('../../utils/dbHelper');
const env = process.env.NODE_ENV || 'development';
const dbConfig = require('../config/config')[env];
const uuid = require('uuid');

module.exports = (sequelize, DataTypes) => {
  var Message = sequelize.define(
    'Message',
    {
      id: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: dbHelper.getDbID,
        primaryKey: true,
      },
      from: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      ChatSessionId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      messageText: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      time: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      deleteByOwner: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      deleteByStranger: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: 'SENT',
      },
      deliveredAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      seenAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    }
    // {
    //   classMethods: {
    //     associate: models => {
    //       Message.belongsTo(models.ChatSession, {
    //         foreignKey: "ChatSessionId"
    //       });
    //     }
    //   }
    // }
  );
  return Message;
};
