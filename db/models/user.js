'use strict';

const bcrypt = require('bcryptjs');
const dbHelper = require('../../utils/dbHelper');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: dbHelper.getDbID,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      mobileNumber: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      age: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      hooks: {
        beforeSave: user => {
          const hash = bcrypt.hashSync(user.password, 10);
          user.password = hash;
          return user;
        },
        beforeCreate: user => {
          const hash = bcrypt.hashSync(user.password, 10);
          user.password = hash;
          return user;
        },
      },
      classMethods: {
        associate: models => {
          User.belongsToMany(models.GameSession, {
            through: models.UserGameSessionMapping,
            foreignKey: 'UserId',
            as: 'gameSessions',
          });
        },
      },
    }
  );
  return User;
};
