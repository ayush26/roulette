'use strict';

module.exports = (sequelize, DataTypes) => {
  const UserGameSessionMapping = sequelize.define('UserGameSessionMapping', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    GameSessionId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    UserId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
  return UserGameSessionMapping;
};
