'use strict';

const dbHelper = require('../../utils/dbHelper');

module.exports = (sequelize, DataTypes) => {
  const GameSession = sequelize.define(
    'GameSession',
    {
      id: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: dbHelper.getDbID,
        primaryKey: true,
      },
      startTime: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      endTime: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      GameId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      hooks: {},
      classMethods: {
        associate: models => {
          GameSession.belongsTo(models.Game, { foreignKey: 'gameId' });
          GameSession.belongsToMany(models.User, {
            through: models.UserGameSessionMapping,
            foreignKey: 'GameSessionId',
            as: 'users',
          });
        },
      },
    }
  );
  return GameSession;
};
