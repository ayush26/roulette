'use strict';

const moment = require('moment-timezone');
const dbHelper = require('../../utils/dbHelper');

module.exports = (sequelize, DataTypes) => {
  const Game = sequelize.define(
    'Game',
    {
      id: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: dbHelper.getDbID,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      minimumBet: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      startTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      endTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      countDownTime: {
        /** In seconds */
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      maximumPlayers: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
      },
    },
    {
      hooks: {
        beforeCreate: game => {
          const diff = moment(game.endTime).diff(game.startTime);
          if (diff < 0) {
            throw new Error('start time should be less than end time');
          }
        },
      },
      // classMethods: {
      //   associate: models => {
      //     User.hasMany(models.PublicId, { foreignKey: "UserId" });
      //   }
      // }
    }
  );
  return Game;
};
