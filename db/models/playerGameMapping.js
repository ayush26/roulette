const dbHelper = require('../../utils/dbHelper');
const env = process.env.NODE_ENV || 'development';
const dbConfig = require('../config/config')[env];
const uuid = require('uuid');

module.exports = (sequelize, DataTypes) => {
  var PlayerGameMapping = sequelize.define(
    'PlayerGameMapping',
    {
      id: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: dbHelper.getDbID,
        primaryKey: true,
      },
      GameId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      PlayerId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      classMethods: {
        associate: models => {
          PlayerGameMapping.belongsTo(models.Game, {
            foreignKey: 'GameId',
          });
          PlayerGameMapping.belongsTo(models.User, {
            foreignKey: 'PlayerId',
          });
        },
      },
    }
  );
  return PlayerGameMapping;
};
