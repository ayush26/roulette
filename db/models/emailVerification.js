const dbHelper = require('../../utils/dbHelper');
const env = process.env.NODE_ENV || 'development';
const dbConfig = require('../config/config')[env];
const uuid = require('uuid');

module.exports = (sequelize, DataTypes) => {
  var EmailVerification = sequelize.define('EmailVerification', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: dbHelper.getDbID,
      primaryKey: true,
    },
    UserId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
  return EmailVerification;
};
