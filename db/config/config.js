const config = {
  development: {
    username: 'root',
    password: 'ayush123',
    database: 'rolet',
    host: 'localhost',
    port: '15432',
    dialect: 'postgres',
    benchmark: true,
    pool: {
      max: 100,
    },
    prefix: 'A',
    schema: 'public',
  },
  staging: {
    username: 'lmnlpjnfbzevbf',
    password: 'b93168e7db31d58b086e82069712da35376041de05923d5b96eee3dc513b3577',
    database: 'dcnafje61j6b2t',
    host: 'ec2-54-243-59-122.compute-1.amazonaws.com',
    dialect: 'postgres',
    benchmark: true,
    pool: {
      max: 100,
    },
    prefix: 'A',
    schema: 'public',
  },
  production: {
    username: process.env.DB_USER || 'events',
    password: process.env.DB_PASSWORD || 'Ayush26',
    database: process.env.DB_NAME || 'eventsdb',
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 5432,
    dialect: 'postgres',
    prefix: 'A',
    schema: 'public',
    pool: {
      max: 100,
      idle: 90000,
    },
  },
};

module.exports = config;
