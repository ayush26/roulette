'use strict';

const redis = require('./redis');
const { GameSession } = require('../controllers/GameSession');

let processingQueue = false;
const findOrCreateActiveGame = async playerData => {
  let game = await redis.getKey('activeGame-1');
  if (!game) {
    console.log('creating new game');
    game = await models.GameSession.create({
      GameId: playerData.gameId,
      status: 'QUEUED',
    });
    game = game.toJSON();
    await redis.setKey('activeGame-1', JSON.stringify(game));
  }
  if (game && typeof game === 'string') game = JSON.parse(game);
  return game;
};

const processPlayer = async playerData => {
  const activeGame = await findOrCreateActiveGame(playerData);
  await models.UserGameSessionMapping.create({ UserId: playerData.user.id, GameSessionId: activeGame.id });
  const gameData = await models.Game.findOne({ where: { id: activeGame.GameId } });
  const totalPLayersArr = JSON.parse((await redis.getKey(`PLAYERSINGAME-${activeGame.id}`)) || '[]');
  totalPLayersArr.push(playerData.user);
  await redis.setKey(`PLAYERSINGAME-${activeGame.id}`, JSON.stringify(totalPLayersArr));
  if (totalPLayersArr.length === gameData.maximumPlayers) {
    // eslint-disable-next-line
    new GameSession({ gameData: gameData.toJSON(), totalPLayers: totalPLayersArr, gameSession: activeGame });
    redis.deleteKey('activeGame-1');
    console.log('start game', activeGame.id);
    await redis.deleteKey('activeGame-1');
  }
};

const startWorkers = async () => {
  // const gameSessions = await models.GameSession.findOne({
  //   where: { id: 'A37fc90cd3ed47cb89ef4dc6d9b0efc' },
  //   include: [
  //     {
  //       model: models.User,
  //       as: 'users',
  //     },
  //   ],
  // });
  // console.log(await gameSessions.users);
  if (processingQueue) {
    /** Some one is processing already */
    return;
  }
  processingQueue = true;
  let queueLength = await redis.getQueueLength('queue-1');
  /* eslint-disable */
  while (queueLength !== 0) {
    const playerData = await redis.popQueue('queue-1');
    const playerDataJSON = playerData && JSON.parse(playerData);
    try {
      await processPlayer(playerDataJSON);
    } catch (e) {
      console.log(e);
    }
    queueLength = await redis.getQueueLength('queue-1');
  }
  processingQueue = false;
  /* eslint-enable */
};

const addToQueue = async (user, game) => {
  const requestId = helper.getUUID();
  await redis.pushQueue('queue-1', JSON.stringify({ gameId: game.id, user, requestId }));
  startWorkers();
  return { requestId };
};

exports.addToQueue = addToQueue;
