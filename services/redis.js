'use strict';

const Promise = require('bluebird');
const redis = Promise.promisifyAll(require('redis'));

const env = process.env.NODE_ENV || 'development';
const config = require('../config/config')[env];

let client;
const init = () => {
  client = redis.createClient(config.redis);
  client.on('error', err => {
    if (err) {
      console.info('Redis server start failed');
    }
  });

  client.on('connect', () => {
    // subscribeToChannels();
    console.log('redis connect');
  });
};

const getKey = key =>
  new Promise((resolve, reject) => {
    client.get(key, (err, res) => {
      if (err) {
        reject(null);
      }
      resolve(res);
    });
  });

const setKey = (key, value) =>
  new Promise((resolve, reject) => {
    client.set(key, value, (err, res) => {
      if (err) {
        console.log('err', err);
        reject(null);
      }
      resolve(res);
    });
  });

const setKeyEx = (key, value, ttl) => client.setexAsync(key, ttl, value);

const setHash = (key, obj) =>
  new Promise((resolve, reject) => {
    client.hmset(key, obj, (err, res) => {
      if (err) {
        reject(null);
      }
      resolve(res);
    });
  });

const getHash = key =>
  new Promise((resolve, reject) => {
    client.hget(key, (err, res) => {
      if (err) {
        reject(null);
      }
      resolve(res);
    });
  });

const deleteKey = key =>
  new Promise((resolve, reject) => {
    client.del(key, err => {
      if (err) {
        reject(false);
      }
      resolve(true);
    });
  });

const pushQueue = (queueName, obj) => client.rpushAsync(queueName, obj);

const popQueue = queueName => client.lpopAsync(queueName);

const getQueueLength = queueName => client.llenAsync(queueName);

const popQueueBulk = (queueName, start, end) => client.lrange(queueName, start, end);

module.exports = {
  init,
  getHash,
  getKey,
  setKeyEx,
  setHash,
  setKey,
  deleteKey,
  pushQueue,
  popQueue,
  getQueueLength,
  popQueueBulk,
};
