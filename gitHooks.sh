DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "${DIR}/"

# Symlink git hooks
for filename in ${DIR}/tmp/gitHooks/*; do
	filename=$(basename ${filename})
  gitFile=".git/hooks/${filename}"
  if ! [ -e $gitFile ]
  then
    ln -sf "${DIR}/tmp/gitHooks/${filename}" ".git/hooks/${filename}"
    chmod +x ".git/hooks/${filename}"
    echo "ok"
  fi
	
done