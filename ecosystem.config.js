module.exports = {
  apps: [
    {
      name: 'eventio-prod',
      script: './app.js',
      watch: true,
    },
  ],
  deploy: {
    production: {
      user: 'ubuntu',
      host: 'ec2-13-126-19-14.ap-south-1.compute.amazonaws.com',
      key: '~/Downloads/AWSMUMKEY.pem',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:ayush2616/eventiobackend.git',
      path: '/home/ubuntu/eventio-current',
      'post-deploy': 'npm install && pm2 startOrRestart ecosystem.config.js',
    },
  },
};
