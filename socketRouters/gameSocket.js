'use strict';

const { activeGames } = require('../controllers/GameSession');

const playerEvent = async (user, data, res) => {
  const gameSession = activeGames[data.gameSessionId];
  if (!gameSession) {
    return res({
      error: true,
      message: 'No Game Session',
    });
  }
  if (data.event === 'TURN' && gameSession.game.type === 'TICTACTOE') {
    gameSession.handlePlayerEvent({ data, user, res });
  }
  return undefined;
};

module.exports = {
  playerEvent,
};
