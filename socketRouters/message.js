const helper = require('../utils/helper');
const redis = require('../services/redis');
const socket = require('../utils/socket');
const R = require('ramda');
const Message = require('../db/models').Message;
const _ = require('lodash');

const sendMessage = async (data, user, res) => {
  const state = { user };
  try {
    await validateMessageReq(data);
    state.chatSession = await getChatSession(data);
    state.message = await createMessageRecord(data, state.chatSession, state.user);
    state.message = await checkUserOnline(state);
    res({ data: state.message });
  } catch (error) {
    res({ error });
  }
};

const getMessages = async (data, user, res) => {
  let result;
  let chatSession;
  const state = { user };
  if (data.chatSessionId) {
    chatSession = await helper.findOneRaw('ChatSession', {
      id: data.chatSessionId,
    });
  }
  if (data.lastTimestamp && chatSession) {
    result = await Message.findAll({
      where: {
        ChatSessionId: chatSession.id,
        createdAt: {
          $lt: data.lastTimestamp,
        },
      },
      order: [['createdAt', 'DESC']],
      limit: data.limit || 100,
    });
    let messages = result.map(row => {
      return deleteMsgKeysForGet(state, chatSession, row.dataValues);
    });
    res({ data: { [chatSession.id]: _.keyBy(messages, 'id') } });
  } else {
    res({
      error: {
        error: true,
        message: 'Bad Request',
      },
    });
  }
};

const validateMessageReq = data => {
  let status = R.allPass([body => R.is(String, body.messageText), body => R.is(String, body.chatSession)])(data);
  if (status) return Promise.resolve(true);
  else
    return Promise.reject({
      error: true,
      errorCode: 'ERR001',
      message: 'unformated data',
    });
};
const getChatSession = data => {
  return helper
    .findOneRaw('ChatSession', {
      id: data.chatSession,
    })
    .then(data => {
      if (!data)
        return Promise.reject({
          error: true,
          errorCode: 'ERR001',
          message: 'unformated data',
        });
      return data;
    });
};
const createMessageRecord = (data, chatSession, user) => {
  if (chatSession.owner === user.id || chatSession.stranger === user.id) {
    return helper.createRaw('Message', {
      from: chatSession.owner === user.id ? 'OWNER' : 'STRANGER',
      ChatSessionId: chatSession.id,
      messageText: data.messageText,
      time: Date.now(),
    });
  }
  return Promise.reject({
    error: true,
    errorCode: 'ERR001',
    message: 'unformated data',
  });
};

const checkUserOnline = state => {
  let otherGuy = state.chatSession.owner == state.user.id ? state.chatSession.stranger : state.chatSession.owner;
  return (
    redis
      .getKey('soc-' + otherGuy)
      .then(val => {
        if (R.isNil(val)) {
          return Promise.reject(state);
        }
        let data = JSON.parse(val);
        state['socketId'] = data.socketId;
        return state;
      })
      // .catch(state =>{
      //   state.message.from = state.chatSession.owner == state.user.id ? "OWNER":"STRANGER";
      //   state['payload'] = {
      //     "message": {
      //       "token": otherGuy,
      //       "data": state.message
      //     }
      //   };
      //   state['token'] = data.fcmId;
      //   state.payload.message['notification']={
      //       "title":"You have a new message",
      //       "body":"Click to see"
      //   };
      // })
      .then(state => {
        let message = deleteCriticalMsgData(state, state.chatSession, R.clone(state.message));
        socket.sendMessage(state.socketId, message);
        return state;
      })
      .then(state => {
        state['message'] = deleteMsgKeysForGet(state, state.chatSession, R.clone(state.message));
        delete state.chatSession;
        return state.message;
      })
      .catch(state => {
        state['message'] = deleteMsgKeysForGet(state, state.chatSession, state.message);
        delete state.chatSession;
        return Promise.resolve(state.message);
      })
  );
};
const deleteCriticalMsgData = (state, chatSession, message) => {
  if (chatSession.owner == state.user.id) {
    //sender is owner
    delete message.deleteByOwner;
  } else {
    delete message.deleteByStranger;
  }
  if (
    (message.from === 'OWNER' && chatSession.owner === state.user.id) ||
    (message.from !== 'OWNER' && chatSession.owner !== state.user.id)
  ) {
    message['type'] = 'RECEIVE';
  } else {
    message['type'] = 'SENT';
  }
  return message;
};
const deleteMsgKeysForGet = (state, chatSession, message) => {
  if (chatSession.owner === state.user.id) {
    //sender is owner
    delete message.deleteByStranger;
  } else {
    delete message.deleteByOwner;
  }
  if (
    (message.from === 'OWNER' && chatSession.owner === state.user.id) ||
    (message.from !== 'OWNER' && chatSession.owner !== state.user.id)
  ) {
    message['type'] = 'SENT';
  } else {
    message['type'] = 'RECEIVE';
  }
  return message;
};

module.exports = {
  sendMessage,
  getMessages,
};
