'use strict';

const env = process.env.NODE_ENV || 'development';
const port = process.env.PORT || 8001;
const express = require('express');
const morgan = require('morgan');
const shell = require('shelljs');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const http = require('http');
const logger = require('./utils/logger');
const helper = require('./utils/helper');
const models = require('./db/models');
const formateDataMiddleware = require('./middlewares/formatData');
const tokenAuthMiddlware = require('./middlewares/tokenAuth');
const corsMiddleware = require('./middlewares/cors');
// const multerMiddleware = require("./middlewares/multer");
const userRouter = require('./routers/user');
const redis = require('./services/redis');
const socketHelper = require('./utils/socket');
const config = require('./config/config')[env];
require('./services/queue-worker');
// const messageRouter = require('./routers/message');

global.models = models;
global.logger = logger;
global.config = config;
global.helper = helper;
global.socketHelper = socketHelper;
global.redis = redis;

const startMiddilware = app => {
  app.use(corsMiddleware);
  app.use(morgan('dev'));
  app.use(helmet());
  app.use((req, res, next) => {
    if (req.is('text/*')) {
      req.body = '';
      req.setEncoding('utf8');
      req.on('data', chunk => {
        req.body += chunk;
      });
      req.on('end', () => {
        try {
          req.body = JSON.parse(req.body);
        } catch (err) {
          console.log('error');
        }
        next();
      });
    } else {
      next();
    }
  });
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(formateDataMiddleware);
  // app.use(multerMiddleware);
  app.use(tokenAuthMiddlware);
};
const startRouters = app => {
  app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/index.html`);
  });
  app.post('/', (req, res) => {
    console.log(req.body);
    res.status(404).send({ success: true });
  });

  // app.get('/', function(req, res){
  //   res.sendFile(__dirname + '/tmp/hello.html');
  // });
  app.get('/db', (req, res) => {
    helper
      .findOneRaw('User', {})
      .then(() => {
        res.status(200).send('UP');
      })
      .catch(err => {
        res.status(200).send(err);
      });
  });
  app.use('/api/v1/users', userRouter);
  // app.use('/api/v1/messages', messageRouter);
};

const initializeSocket = _http => {
  socketHelper.initSocket(_http);
};
const initializeGitHooks = () => {
  if (env === 'development') {
    shell.exec('sh gitHooks.sh', code => {
      if (code === 0) {
        console.log('Git hooks created');
      } else {
        console.log('Git hooks failed');
      }
    });
  }
};
const startServer = () => {
  initializeGitHooks();
  const app = express();
  const server = http.createServer(app);
  initializeSocket(server);
  models.sequelize.sync();
  redis.init();
  startMiddilware(app);
  startRouters(app);
  server.listen(port, () => {
    logger.info(`Server listening on port ${port} ${new Date()}`);
  });
};

startServer();
