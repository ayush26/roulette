module.exports = {
  extends: ['airbnb-base', 'prettier'],
  env: {
    node: true,
  },
  plugins: ['import'],
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'script',
    ecmaFeatures: {
      modules: false,
      experimentalObjectRestSpread: true,
    },
  },
  rules: {
    'prefer-promise-reject-errors': 'off',
    'no-unexpected-multiline': 'off',
    'no-else-return': 'off',
    'no-restricted-syntax': 'off',
    'max-len': 'off',
    'func-names': 'off',
    'import/no-dynamic-require': 'off',
    'no-plusplus': [
      'error',
      {
        allowForLoopAfterthoughts: true,
      },
    ],
    'no-param-reassign': [
      'error',
      {
        props: false,
      },
    ],
    camelcase: [
      'error',
      {
        properties: 'never',
      },
    ],
    'no-underscore-dangle': ['off'],
    strict: [2, 'global'],
    'no-use-before-define': [
      'error',
      {
        functions: false,
      },
    ],
  },
  globals: {
    models: true,
    config: true,
    helper: true,
    logger: true,
    socketHelper: true,
    redis: true,
  },
};
