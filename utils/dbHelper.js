const env = process.env.NODE_ENV || 'development';
const models = require('../db/models');
const R = require('ramda');
const config = require('../config/config')[env];
const uuid = require('uuid');
const jwt = require('jsonwebtoken');
const dbConfig = require('../db/config/config')[env];

const getDbID = () => {
  return (
    dbConfig.prefix +
    uuid
      .v4()
      .substr(1, 34)
      .replace(/-/g, '')
  );
};

module.exports.getDbID = getDbID;
