'use strict';

const env = process.env.NODE_ENV || 'development';
const path = require('path');
const R = require('ramda');
const nodemailer = require('nodemailer');
const _ = require('lodash');
const uuid = require('uuid');
const jwt = require('jsonwebtoken');
const models = require('../db/models');
const config = require('../config/config')[env];
const dbConfig = require('../db/config/config')[env];
const redis = require('../services/redis');

// const AWS = require('aws-sdk');
/** ******************************************** Db functions**************************** */
const findOneRaw = (entity, where) => {
  if (entity && where) {
    return models[entity]
      .findOne({
        where,
      })
      .then(x => {
        if (x && x.dataValues) return x.dataValues;
        else return null;
      })
      .catch(err => {
        console.log(err);
        return null;
      });
  }
  return undefined;
};

const findOne = (state, key, entity, where) =>
  findOneRaw(entity, where).then(data => {
    if (data == null) return Promise.reject(state);
    const x = {};
    x[key] = data;
    return R.merge(state, x);
  });

const updateRaw = (entity, values, where) =>
  models[entity]
    .update(values, {
      where,
      returning: true,
    })
    .then(result => {
      if (result && Array.isArray(result) && result.length > 1) {
        const newResult = R.map(x => x.dataValues, result[1]);
        return newResult.length === 1 ? newResult[0] : newResult;
      } else {
        console.log('Error updating record ', entity, values, where);
        return null;
      }
    });

const findAllRaw = (entity, where, include) => {
  let includeArr = [];
  if (include) {
    includeArr = R.map(item => models[item], include);
  }
  if (entity && where) {
    return models[entity]
      .findAll({
        where,
        include: includeArr,
      })
      .then(x => R.map(result => result.dataValues, x))
      .catch(err => {
        console.log(err);
        return Promise.reject(null);
      });
  }
  return null;
};

const findAll = (state, key, entity, values, include) =>
  findAllRaw(entity, values, include).then(data => {
    if (data == null) return Promise.reject(state);
    const x = {};
    x[key] = data;
    return R.merge(state, x);
  });

const deleteRaw = (entity, where) => {
  if (entity && where) {
    return models[entity]
      .destroy({
        where,
        returning: true,
      })
      .then(x => `${x}`);
  }
  return undefined;
};

const createRaw = (entity, values) => {
  if (entity && values) {
    return models[entity]
      .create(values)
      .then(data => data.dataValues)
      .catch(err => {
        console.log('DB error', err);
        return Promise.reject(err);
      });
  }
  return undefined;
};

const createBulkRaw = (entity, value) =>
  models[entity].bulkCreate(value).then(result => {
    if (result && R.is(Object, result)) {
      const dataValues = [];
      for (const item of result) {
        dataValues.push(item.dataValues);
      }
      return dataValues;
    } else {
      console.log('Error creating bulk records ', entity, value);
      return null;
    }
  });

const createRecord = (state, key, entity, values) =>
  createRaw(entity, values).then(data => {
    if (data == null) return Promise.reject(state);
    const x = {};
    x[key] = data;
    return R.merge(state, x);
  });

const createBulkRecord = (state, key, entity, values) =>
  createBulkRaw(entity, values).then(data => {
    if (data == null) return Promise.reject(state);
    const x = {};
    x[key] = data;
    return Promise.resolve(R.merge(state, x));
  });

/** *******************************************DB FUNCTION END*********************************************** */

const getJWT = value => {
  const token = jwt.sign(value, config.secret_hash, {
    expiresIn: 2628000,
  });
  return token;
};

const verifyJWT = (state, token) =>
  new Promise((resolve, reject) => {
    jwt.verify(token, config.secret_hash, (err, decoded) => {
      if (err) reject(R.merge(state, { error: true, message: 'Token expired' }));
      else resolve(R.merge(state, { decoded }));
    });
  });

const getDbID = () =>
  dbConfig.prefix +
  uuid
    .v4()
    .substr(1, 34)
    .replace(/-/g, '');

const blackListKeys = (state, keys) => {
  let newState = R.clone(state);
  keys.map(key => {
    if (Array.isArray(key)) {
      newState = R.dissocPath(key, state);
    } else {
      delete newState[key];
    }
    return null;
  });
  return newState;
};

const whiteListKeys = (state, keys, startState) => {
  let newState = startState || {};
  keys.map(key => {
    if (Array.isArray(key) && R.path(key, state) !== undefined) {
      newState = R.assocPath(key, R.path(key, state), newState);
    } else if (state[key] !== undefined) {
      newState[key] = state[key];
    }
    return null;
  });
  return newState;
};

const getWhitelistedKeys = (state, keys) => {
  if (keys.default === 'WHITELISTALL') {
    const newState = blackListKeys(state, keys.blackList);
    return whiteListKeys(state, keys.whiteList || [], newState);
  } else if (keys.default === 'BLACKLISTALL') {
    const newState = whiteListKeys(state, keys.whiteList);
    return blackListKeys(newState, keys.blackList || []);
  }
  return state;
};

/* eslint-disable */
const uploadToAws = (state, key, file) => {
  return new Promise((resolve, reject) => {
    s3.putObject(
      {
        Bucket: config.bucketName || 'eventio-files-ayush',
        Key: key + getFileExtension(file.originalname),
        Body: file.buffer,
        ACL: 'public-read', // your permisions
        Metadata: {
          'Content-Type': file.mimetype,
        },
      },
      err => {
        if (err) {
          reject(R.merge(state, { error: true, message: 'upload failed' }));
        }
        resolve(state);
      }
    );
  });
};
/* eslint-enable */

const getUUID = () => uuid.v4();

const getFileExtension = fileName => path.extname(fileName);

const emailTransporter = nodemailer.createTransport({
  host: config.emailProviderHost,
  port: 25,
  secure: false,
  tls: {
    rejectUnauthorized: false,
  },
  auth: {
    user: config.emailProviderUsername,
    pass: config.emailProviderPassword,
  },
});

const sendMail = mailOptions => {
  emailTransporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log(`Email sent: \n ${info.response}`);
    }
  });
};

const catchReqError = (err, res) => {
  if (err.httpCode) {
    res.status(err.httpCode).send(err);
  }
  logger.error('Error - ', err);
  res.status(500).send(_.pick(err, ['message', 'original']));
};

const setGameState = (state, gameSessionId) =>
  redis.setKeyEx(`GAMESTATE-${gameSessionId}`, JSON.stringify(state), 86400);

const getGameState = async gameSessionId => {
  const a = await redis.getKey(`GAMESTATE-${gameSessionId}`);
  return a && JSON.parse(a);
};

const getPlayerInGame = async gameSessionId => {
  const a = await redis.getKey(`PLAYERSINGAME-${gameSessionId}`);
  return a && JSON.parse(a);
};

const notifyPlayerInGame = async (gameSessionId, players, msg) => {
  const playerSockets = await Promise.all(players.map(player => redis.getKey(`soc-${player}`)));
  playerSockets.forEach(socket => {
    const socketJSON = socket && JSON.parse(socket);
    socketHelper.sendMessage(socketJSON.socketId, msg);
  });
};

module.exports = {
  findOneRaw,
  findOne,
  findAllRaw,
  updateRaw,
  deleteRaw,
  createRaw,
  createBulkRaw,
  createRecord,
  createBulkRecord,
  getJWT,
  getDbID,
  verifyJWT,
  getWhitelistedKeys,
  uploadToAws,
  getUUID,
  getFileExtension,
  findAll,
  sendMail,
  catchReqError,
  setGameState,
  getGameState,
  getPlayerInGame,
  notifyPlayerInGame,
};
