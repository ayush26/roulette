var admin = require('firebase-admin');

var serviceAccount = require('../secureKeys/serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://incognito-001.firebaseio.com',
});

const sendMessage = (state, token) => {
  // delete state.payload.message.token;
  let t1 = Date.now();
  return admin
    .messaging()
    .sendToDevice(
      token,
      {
        data: { data: JSON.stringify(state.payload.message.data) },
        webpush: {
          headers: {
            Urgency: 'high',
          },
        },
      },
      { priority: 'high' }
    )
    .then(function(response) {
      console.log(Date.now() - t1);
      return state;
    })
    .catch(function(error) {
      console.log(error);
      return Promise.reject(state);
    });
};

module.exports = {
  sendMessage: sendMessage,
};
