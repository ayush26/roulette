const Sender = require('node-xcs').Sender;
const Result = require('node-xcs').Result;
const Message = require('node-xcs').Message;
const Notification = require('node-xcs').Notification;
const helper = require('./helper');

const xcs = new Sender('869670988803', 'AIzaSyD3o-Wwp5FbfPy9ThjJEmAGvUq1bOknOJE');

xcs.on('connected', d => {
  console.log('Firebase connected' + d);
});
xcs.on('disconnected', d => {
  console.log('Firebase disconnected' + d);
});
xcs.on('online', d => {
  console.log('Firebase online' + d);
});
xcs.on('error', d => {
  console.log('Firebase error' + d);
});

xcs.on('receipt', function(messageId, from, data, category) {
  console.log('rec', from, data);
});

const send = () => {
  var notification = new Notification('ic_launcher')
    .title('Hello buddy!')
    .body('node-xcs is awesome.')
    .build();
  var message = new Message('messageId_1046')
    .priority('high')
    .dryRun(false)
    .addData('messageText', {
      id: 'A5ab45857eca414494a5a4027b41ec9',
      visibleToStranger: true,
      blockedByOwner: false,
      blockedByStranger: false,
      stranger: 'A73828e8a4d74a46a59239c28df1293',
      ownerName: 'sdsdsd',
      updatedAt: '2018-02-08T18:01:04.139Z',
      createdAt: '2018-02-08T18:01:04.139Z',
    })
    .deliveryReceiptRequested(true)
    .build();

  // Only fired for messages where options.delivery_receipt_requested = true

  let to =
    'dTFCH6BRSNo:APA91bE4IYl-4T0yHdWXFUx16pClJBA4vRezEwAwDhal3IMJKe2zE__tAVsgHadiGZqHF4TX7zjHDjMMLbbT30BeZrhf1EZCBzk2SuKDZd0kFL0NeUAHU97ZbBSbZny2uLHj2q-gTXlN';
  return new Promise((res, rev) => {
    xcs.sendNoRetry(message, to, function(result) {
      if (result.getError()) {
        console.error(result.getError());
        rev();
      } else {
        console.log('message sent: #' + result.getMessageId());
        res();
      }
    });
  });
};

const sendMessage = (state, token, messageText) => {
  // delete state.payload.message.token;
  return new Promise((resolve, reject) => {
    let message = new Message('messageId' + helper.getUUID())
      .priority('high')
      .dryRun(false)
      .addData('messageBody', messageText)
      .build();
    xcs.sendNoRetry(message, token, function(result) {
      if (result.getError()) {
        console.error(result.getError());
        reject(state);
      } else {
        console.log('message sent: #' + result.getMessageId());
        resolve(state);
      }
    });
  });
};

// module.exports ={
//   sendMessage:sendMessage
// }

module.exports = {
  send: send,
  sendMessage: sendMessage,
};
