'use strict';

const io = require('socket.io');
const redisAdapter = require('socket.io-redis');
const helper = require('./helper');
const redis = require('../services/redis');
const logger = require('../utils/logger');
const gameSocketRouter = require('../socketRouters/gameSocket');

const env = process.env.NODE_ENV || 'development';
const config = require('../config/config')[env];

let socket;

const initMiddleware = () => {
  // eslint-disable-next-line
  socket.use(async (socket, next) => {
    if (socket.request.headers.cookie) {
      // eslint-disable-next-line
      const parsedCookies = parseCookies(socket.request.headers.cookie);
      if (parsedCookies && parsedCookies.__tn) {
        helper
          .verifyJWT({}, parsedCookies.__tn)
          .then(async state => {
            const user = await models.User.findOne({
              where: {
                id: state.decoded.id,
              },
            });
            if (user) {
              socket.state = { user };
              next();
            } else {
              next(
                new Error(
                  JSON.stringify({
                    action: 'RESET',
                  })
                )
              );
            }
          })
          .catch(err => {
            next(new Error(JSON.stringify(err)));
          });
      } else {
        next(
          new Error(
            JSON.stringify({
              action: 'RESET',
              message: 'No token',
              code: 401,
            })
          )
        );
      }
    }
  });
};

// eslint-disable-next-line
const onConnection = async socket => {
  console.log('connected', socket.id);
  if (true || (socket.state && socket.state.user)) {
    await redis.setKey(
      `soc-${socket.state.user.id}`,
      JSON.stringify({
        socketId: socket.id,
      })
    );
    // getUserById(cookies['uid']).then(user => {
    //   socket.broadcast.emit('onlineStateChange',{user:user,online:true});
    // });
    // getAllUsers({},socket);
    // console.log(clients);
    socket.on('addToGame', res => {
      console.log('hey');
      return gameSocketRouter.addToGame(socket.state.user, res);
    });
    socket.on('playerEvent', (data, res) => gameSocketRouter.playerEvent(socket.state.user, data, res));
    // socket.on('chat_message', (data /** , res */) => {});
    // socket.on('getAllUsers',data =>getAllUsers(data,socket));
    // socket.on('getMessages', (data, res) => messageRouter.getMessages(data, socket.state.user, res));
    socket.on('hi', (data, res) => {
      console.log(data);
      res('from server');
    });
    // setInterval(() => {
    //   socket.emit("dummy", {
    //     message: "HEllooo",
    //     type: Math.floor(Math.random() * 100) % 2 === 0 ? "RECEIVE" : "SEND"
    //   });
    // }, 1000);
  }
  socket.on('disconnect', async () => {
    // socket.broadcast.emit("onlineStateChange", {
    //   userId: sockets[socket.id],
    //   online: false
    // });
    if (socket.state && socket.state.user) {
      console.log('disconnected', socket.state.user.id);

      await redis.deleteKey(`soc-${socket.state.user.id}`);
    }
  });
};

// const getAllUsers = (data, soc) => {
//   helper
//     .findAll({}, "users", "User", {})
//     .then(state => {
//       state.users = R.map(user => {
//         user["online"] = R.isNil(clients[user.id]) ? false : true;
//         return user;
//       }, state.users);
//       return state;
//       //socket.to(soc.id).emit("allUsers",state.users);
//     })
//     .then(state => getLastMessages(state, soc))
//     .then(state => {
//       socket.to(soc.id).emit("allUsers", state.users);
//     });
// };

const sendMessage = (socketId, data) => {
  // console.log(socket);
  // socket.sockets.emit('data', 'data');
  socket.to(socketId).emit('data', data);
};
const parseCookies = cookies => {
  const list = {};
  cookies.split(';').forEach(cookie => {
    const parts = cookie.split('=');
    list[parts.shift().trim()] = decodeURI(parts.join('='));
  });
  return list;
};

const initSocket = http => {
  socket = io.listen(http);
  socket.adapter(redisAdapter({ host: config.redis.host, port: config.redis.port }));
  initMiddleware();
  logger.info(`Socket started at ${Date.now()} `);
  socket.on('connection', _socket => onConnection(_socket));
};

module.exports = {
  initSocket,
  sendMessage,
};
// const messageRouter = require('../socketRouters/message');
