'use strict';

const logger = require('winston');
// fs.mkdir('./tempLogs', function(err) {
//   if (err) throw err;
// });
logger.setLevels({ error: 0, warn: 1, silly: 2, info: 3, debug: 4 });
logger.addColors({
  debug: 'green',
  info: 'cyan',
  silly: 'magenta',
  warn: 'yellow',
  error: 'red',
});
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, { level: 'debug', colorize: true });
logger.add(logger.transports.File, {
  name: 'error',
  filename: './tempLogs/project-error.log',
  maxsize: 1024 * 1024 * 10, // 10MB
  level: 'warn',
});
logger.add(logger.transports.File, {
  name: 'info',
  filename: './tempLogs/app.log',
  maxsize: 1024 * 1024 * 10, // 10MB
  level: 'info',
});
module.exports = logger;
